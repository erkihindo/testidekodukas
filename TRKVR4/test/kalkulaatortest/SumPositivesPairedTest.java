package kalkulaatortest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import kalkulaator.SumPositivesPaired;;

public class SumPositivesPairedTest {
	@Test
	public void test()
	{
		assertEquals("Result is different then expected1",  2, SumPositivesPaired.addPos(new int[] {2}));
		assertEquals("Result is different then expected2",  12, SumPositivesPaired.addPos(new int[] {2, 4, 6}));
		assertEquals("Result is different then expected3",  12, SumPositivesPaired.addPos(new int[] {-2, -4, 2, 4, 6}));
		assertEquals("Result is different then expected4",  12, SumPositivesPaired.addPos(new int[] {1, 2, 4, 6}));
		assertEquals("Result is different then expected5",  12, SumPositivesPaired.addPos(new int[] {-2, -4, 1, 2, 4, 6}));
	}
}
