package kalkulaator;

public class SumPositivesPaired {
	public static int addPos(int[] list) {
		int result = 0;
		for(int i = 0; i < list.length; i++) {
			if(list[i] > 0 && list[i]%2 == 0) {
				result += list[i];
			}
		}
		return result;
	}
}
